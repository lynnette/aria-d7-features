<?php
/**
 * @file
 * aria_security.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function aria_security_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access security review list'.
  $permissions['access security review list'] = array(
    'name' => 'access security review list',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'security_review',
  );

  // Exported permission: 'run security checks'.
  $permissions['run security checks'] = array(
    'name' => 'run security checks',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'security_review',
  );

  return $permissions;
}
