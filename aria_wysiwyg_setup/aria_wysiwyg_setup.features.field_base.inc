<?php
/**
 * @file
 * aria_wysiwyg_setup.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function aria_wysiwyg_setup_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_folder'.
  $field_bases['field_folder'] = array(
    'active' => 1,
    'bundle' => 'image',
    'cardinality' => 1,
    'deleted' => 0,
    'entity_type' => 'file',
    'entity_types' => array(),
    'field_name' => 'field_folder',
    'indexes' => array(
      'tid' => array(
        0 => 'tid',
      ),
    ),
    'label' => 'Media Folder',
    'locked' => 0,
    'module' => 'taxonomy',
    'required' => TRUE,
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => 'media_folders',
          'parent' => 0,
        ),
      ),
    ),
    'translatable' => 0,
    'type' => 'taxonomy_term_reference',
  );

  return $field_bases;
}
