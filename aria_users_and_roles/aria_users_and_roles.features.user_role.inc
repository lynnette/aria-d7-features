<?php
/**
 * @file
 * aria_users_and_roles.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function aria_users_and_roles_user_default_roles() {
  $roles = array();

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 2,
  );

  // Exported role: pre auth registration.
  $roles['pre auth registration'] = array(
    'name' => 'pre auth registration',
    'weight' => 2,
  );

  // Exported role: site administrator.
  $roles['site administrator'] = array(
    'name' => 'site administrator',
    'weight' => 4,
  );

  // Exported role: site editor.
  $roles['site editor'] = array(
    'name' => 'site editor',
    'weight' => 5,
  );

  return $roles;
}
